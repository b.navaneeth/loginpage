const Connect = require("./connect.js");

let data = `CREATE TABLE IF NOT EXISTS users_data (
    userId INT PRIMARY KEY AUTO_INCREMENT, 
    Name TEXT,  
    email VARCHAR(100) UNIQUE, 
    password TEXT
)`;

Connect.getConnection((err, connection) => {
    if (err) {
        console.log(err);
    } else {
        connection.query(data, (err, result, fields) => {
            if (err) {
                console.log(err);
                Connect.end();
            } else {
                console.log("Table created");
                Connect.end();
            }
            connection.release();
        });
    }
});