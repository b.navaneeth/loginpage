const { host, user, password, database } = require("../config");
const mysql = require("mysql");
const database_connect = mysql.createPool({
    host: host,
    user: user,
    password: password,
    database: database,
});

module.exports = database_connect;