const Connect = require("./connect.js");

module.exports = function(data) {
    return new Promise((resolve, reject) => {
        let userData = {
            name: data.user_name,
            email: data.email,
            password: data.password,
        };
        let insert = `INSERT INTO users_data SET ?`;

        Connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(insert, userData, (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                    connection.release();
                });
            }
        });
    });
};