const Connect = require("./connect.js");

module.exports = function(data) {
    return new Promise((resolve, reject) => {
        let email = data.email;
        let password = data.password;
        let insert = `SELECT email FROM users_data WHERE email=? AND password=?`;

        Connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(insert, [email, password], (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        if (data.length == 0) {
                            resolve(false);
                        } else {
                            resolve(true);
                        }
                    }
                    connection.release();
                });
            }
        });
    });
};