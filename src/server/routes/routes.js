const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();

let urlencodedParser = bodyParser.urlencoded({ extended: false });

router.get("/home", (req, res) => {
    if (req.session.email) {
        res.status(200);
        res.render("Home", {
            email: req.session.email,
        });
    } else {
        res.redirect("/");
    }
});

router.post("/logout", (req, res) => {
    req.session.destroy();
    res.redirect("/");
});

module.exports = router;