const express = require("express");
const bodyParser = require("body-parser");
const { check, validationResult } = require("express-validator");
const checkPassword = require("../DataBaseFunctions/checkPassword.js");
const checkUser = require("../DataBaseFunctions/checkUser");
const router = express.Router();

let urlencodedParser = bodyParser.urlencoded({ extended: false });

router.get("/", (req, res) => {
    if (!req.session.email) {
        res.status(200);
        res.render("index");
    } else {
        res.redirect("/home");
    }
});

router.post(
    "/",
    urlencodedParser, [check("email", "Not a valid email").isEmail()],
    async(req, res, next) => {
        try {
            let errors = validationResult(req);
            if (!errors.isEmpty()) {
                let alert = errors.array();
                res.status(400);
                res.render("index", {
                    alert,
                });
            } else {
                if (await checkUser(req.body)) {
                    if (await checkPassword(req.body)) {
                        req.session.email = req.body.email;
                        res.redirect("/home");
                    } else {
                        res.status(400);
                        res.render("index", {
                            alert: [{ msg: "Invalid Password" }],
                        });
                    }
                } else {
                    res.status(400);
                    res.render("index", {
                        alert: [{ msg: "Email id not found try signup" }],
                    });
                }
            }
        } catch (err) {
            next(err);
        }
    }
);

module.exports = router;