const express = require("express");
const bodyParser = require("body-parser");
const { check, validationResult } = require("express-validator");
const addUser = require("../DataBaseFunctions/addUser");
const checkUser = require("../DataBaseFunctions/checkUser");
const router = express.Router();

let urlencodedParser = bodyParser.urlencoded({ extended: false });

router.get("/", (req, res) => {
    if (!req.session.email) {
        res.status(200);
        res.render("signup");
    } else {
        res.redirect("/home");
    }
});

router.post(
    "/",
    urlencodedParser, [
        check("email", "Not a valid email").isEmail(),
        check(
            "password",
            "Password must contain minimum 8 character, At least one uppercase.,At least one lower case,At least one special character."
        ).isStrongPassword(),
    ],
    async(req, res, next) => {
        try {
            let errors = validationResult(req);
            if (!errors.isEmpty()) {
                let alert = errors.array();
                res.status(400);
                res.render("signup", {
                    alert,
                });
            } else {
                if (await checkUser(req.body)) {
                    res.status(400);
                    res.render("signup", {
                        alert: [{ msg: "User already exists" }],
                    });
                } else {
                    await addUser(req.body);
                    req.session.email = req.body.email;
                    res.redirect("/home");
                }
            }
        } catch (err) {
            next(err);
        }
    }
);

module.exports = router;