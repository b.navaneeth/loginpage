const express = require("express");
const path = require("path");
const app = express();
const ejs = require("ejs");
const session = require("express-session");
const { secretCode, port } = require("./config.js");
const router = require("./routes/routes.js");
const routerLogin = require("./routes/routes-login");
const routerSignup = require("./routes/routes-signup");

app.use(express.static(`${__dirname}/../public/views`));

app.set("views", path.join(__dirname, "../public/views"));
app.set("view engine", "ejs");

app.use(
    session({
        secret: secretCode,
        resave: false,
        saveUninitialized: true,
        cookie: { maxAge: 1000 * 60 * 60 },
    })
);

app.use(router);

app.use(routerLogin);

app.use("/signup", routerSignup);

app.use((req, res) => {
    res.status(404);
    res.render("pagenotfound");
});

app.use(function(err, req, res, next) {
    console.log(err);
    res.status(500);
    res.render("error");
});

app.listen(port);