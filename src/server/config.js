const dotenv = require("dotenv");
dotenv.config({
    path: "../../.env",
});
module.exports = {
    port: process.env.PORT,
    host: process.env.HOST,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    secretCode: process.env.SECRET,
};